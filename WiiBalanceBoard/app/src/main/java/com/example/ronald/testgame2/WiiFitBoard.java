package com.example.ronald.testgame2;

import android.app.ListActivity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothHeadset;
import android.bluetooth.BluetoothHealth;
import android.bluetooth.BluetoothProfile;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.os.Handler;
import android.os.Parcel;
import android.os.ParcelUuid;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Method;
import java.util.UUID;

/**
 * Created by Ronald on 30-9-2014.
 */
public class WiiFitBoard extends ListActivity /*implements BluetoothAdapter.LeScanCallback*/ {

    private static final String TAG = WiiFitBoard.class.getSimpleName();
    private static final int CONTROL_CHANNEL = 0x11;
    private static final int DATA_CHANNEL = 0x13;


    private BluetoothHealth bluetoothHealth;
    private Handler mHandler;
    private boolean mScanning;

    private BluetoothSocket controleSocket;
    private BluetoothSocket dataSocket;

    private InputStream inputStream;
    private OutputStream outputStream;

    private BluetoothAdapter bluetoothAdapter;
    private BluetoothSocket bluetoothSocket;
    private BluetoothDevice bluetoothDevice;
    private BluetoothGatt bluetoothGatt;
    private ParcelUuid[] uuids;

    public WiiFitBoard(BluetoothDevice bld, BluetoothAdapter bla){
        this.bluetoothDevice = bld;
        this.bluetoothAdapter = bla;

       // bluetoothSocket = WiimoteSocket.create(bld, 0x13);
//        createSocket(bld);
//        bluetoothSocket.getRemoteDevice().setPairingConfirmation(true);
//        ConnectThread connectThread = new ConnectThread();
//        connectThread.run();
        scanLeDevice(true);
    }


    public void scanLeDevice(final boolean enable){
        if(enable){
            mHandler.postDelayed(new Runnable(){
                @Override
                public void run(){
                    mScanning = false;
                    bluetoothAdapter.stopLeScan(leScanCallback);
                };
            }, 10000);
        }
        mScanning = true;
        bluetoothAdapter.startLeScan(leScanCallback);
    }

    private BluetoothAdapter.LeScanCallback leScanCallback = new BluetoothAdapter.LeScanCallback() {
        @Override
        public void onLeScan(BluetoothDevice bluetoothDevice, int i, byte[] bytes) {
            if(bluetoothDevice.getName().equals("Nintendo RVL-WBC-01")){
                System.out.println("Device found");
            }else{
                System.out.println("Device not found");
            }
        }
    };

    public void createSocket(BluetoothDevice bld){
        try{
            controleSocket = WiimoteSocket.create(bld, DATA_CHANNEL);
            controleSocket.connect();
            outputStream = controleSocket.getOutputStream();

            dataSocket = WiimoteSocket.create(bld, CONTROL_CHANNEL);
            dataSocket.connect();
            inputStream = dataSocket.getInputStream();

            if(bluetoothSocket.isConnected()){
                Log.d(TAG, "Connected");
            }
        }catch(Exception e ){
            Log.d(TAG, e.getMessage());
        }
    }

    public void pin(BluetoothDevice bld){
        try {
            Method setPin = BluetoothDevice.class.getDeclaredMethod("setPin", byte[].class);
            setPin.setAccessible(true);
            String addr = bluetoothAdapter.getAddress();
            Log.d(TAG, "Attempting to pair using adapter " + addr);
            byte[] pin = new byte[6];
//            pin[0] = Byte.parseByte(addr.substring(15, 17), 16);
//            pin[1] = Byte.parseByte(addr.substring(12, 14), 16);
//            pin[2] = Byte.parseByte(addr.substring(9, 11), 16);
//            pin[3] = Byte.parseByte(addr.substring(6, 8), 16);
//            pin[4] = Byte.parseByte(addr.substring(3, 5), 16);
//            pin[5] = Byte.parseByte(addr.substring(0, 2), 16);

            pin[0] = Byte.parseByte("0x42");
            pin[1] = Byte.parseByte("0x9B");
            pin[2] = Byte.parseByte("0x43");
            pin[3] = Byte.parseByte("0x59");
            pin[4] = Byte.parseByte("0x26");
            pin[5] = Byte.parseByte("0x00");

            setPin.invoke(bld, pin);
        } catch (Exception ex) {
            Log.e(TAG, "Failed to pair", ex);
        }
    }

    private class ConnectThread extends Thread {

        public void run() {
            try {
                //pin(bluetoothDevice);
                bluetoothSocket.connect();
            } catch (IOException e) {
                Log.d(TAG, e.getMessage());
            }
        }
    }

//    public void connectToDevice(BluetoothDevice bld){
//        try {
//            bld.connectGatt(this, true, bluetoothGattCallback);
//            mStartRunnable.run();
//        }catch (Exception e){
//            Log.d(TAG, e.getMessage());
//        }
//    }
//
//    private BluetoothGattCallback bluetoothGattCallback = new BluetoothGattCallback() {
//        @Override
//        public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
//            super.onConnectionStateChange(gatt, status, newState);
//            Log.d(TAG, gatt.getDevice().getName());
//        }
//
//        @Override
//        public void onServicesDiscovered(BluetoothGatt gatt, int status) {
//            super.onServicesDiscovered(gatt, status);
//            Log.d(TAG, gatt.getDevice().getName());
//        }
//
//        @Override
//        public void onCharacteristicRead(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
//            super.onCharacteristicRead(gatt, characteristic, status);
//            Log.d(TAG, gatt.getDevice().getName());
//        }
//
//        @Override
//        public void onCharacteristicWrite(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
//            super.onCharacteristicWrite(gatt, characteristic, status);
//
//        }
//
//        @Override
//        public void onCharacteristicChanged(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {
//            super.onCharacteristicChanged(gatt, characteristic);
//        }
//
//        @Override
//        public void onDescriptorRead(BluetoothGatt gatt, BluetoothGattDescriptor descriptor, int status) {
//            super.onDescriptorRead(gatt, descriptor, status);
//        }
//
//        @Override
//        public void onDescriptorWrite(BluetoothGatt gatt, BluetoothGattDescriptor descriptor, int status) {
//            super.onDescriptorWrite(gatt, descriptor, status);
//        }
//
//        @Override
//        public void onReliableWriteCompleted(BluetoothGatt gatt, int status) {
//            super.onReliableWriteCompleted(gatt, status);
//        }
//
//        @Override
//        public void onReadRemoteRssi(BluetoothGatt gatt, int rssi, int status) {
//            super.onReadRemoteRssi(gatt, rssi, status);
//        }
//    };
//
//    private Runnable mStartRunnable = new Runnable() {
//        @Override
//        public void run() {
//          //  startScan();
//        }
//    };
//
//    private void startScan() {
//        bluetoothAdapter.startLeScan(this);
//    }
//
//
//    @Override
//    public void onLeScan(BluetoothDevice bluetoothDevice, int i, byte[] bytes) {
//        System.out.println(bluetoothDevice.getAddress());
//        String s = "";
//        for(byte b : bytes){
//            s = s +  String.valueOf(b);
//        }
//        Log.i(TAG, "New LE Device: " + bluetoothDevice.getName() + " @ " + i + " Bytes = " + s);
//        s = "";
//    }
//
//    public void readSensor(BluetoothGatt gatt){
//        BluetoothGattCharacteristic bluetoothGattCharacteristic;
//
//       // bluetoothGattCharacteristic = gatt.getService();
//    }
}


