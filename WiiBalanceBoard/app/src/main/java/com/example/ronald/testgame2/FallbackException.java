package com.example.ronald.testgame2;

/**
 * Created by Ronald on 6-10-2014.
 */
public class FallbackException extends Exception {

    private static final long serialVersionUID = 1L;

    public FallbackException(Exception e) {
        super(e);
    }

}
