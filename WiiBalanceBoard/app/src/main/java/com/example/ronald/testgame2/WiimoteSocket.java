package com.example.ronald.testgame2;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.os.ParcelUuid;

import java.lang.reflect.Constructor;
import java.util.UUID;

/**
 * Created by Ronald on 3-10-2014.
 */
public class WiimoteSocket {

    public static BluetoothSocket create(BluetoothDevice device, int port)
    {
        try {
//            /*
//             * BluetoothSocket(int type, int fd, boolean auth, boolean encrypt, BluetoothDevice device, int port, ParcelUuid uuid)
//             */
//            Constructor<BluetoothSocket> construct = BluetoothSocket.class.getDeclaredConstructor(int.class, int.class, boolean.class,
//                    boolean.class, BluetoothDevice.class, int.class, ParcelUuid.class);
//
//            construct.setAccessible(true);
//
//            return construct.newInstance(3 /* TYPE_L2CAP */, -1, false, false, dev, port, null);
//
//           //BluetoothSocket tmp = dev.createRfcommSocketToServiceRecord(UUID.fromString("00001101-0000-1000-8000-00805F9B34FB"));
//
//           //return new NativeBluetoothSocket(tmp);

            int type = 3; // L2CAP protocol
            int fd = -1; // Create a new socket
            boolean auth = false; // No authentication
            boolean encrypt = false; // Not encrypted

            try {
                Constructor<BluetoothSocket> constructor = BluetoothSocket.class.getDeclaredConstructor(int.class,
                        int.class, boolean.class, boolean.class, BluetoothDevice.class, int.class, ParcelUuid.class);
                constructor.setAccessible(true);
                BluetoothSocket clientSocket = (BluetoothSocket) constructor.newInstance(type, fd, auth, encrypt, device,
                        port, null);
                return clientSocket;
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        } catch (Exception ex) {
            return null;
        }
    }
}
