package com.example.ronald.testgame2;

import android.annotation.SuppressLint;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.MediaRouteActionProvider;
import android.support.v7.media.MediaRouteSelector;
import android.support.v7.media.MediaRouter;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Toast;

import com.google.android.gms.cast.ApplicationMetadata;
import com.google.android.gms.cast.Cast;
import com.google.android.gms.cast.CastDevice;
import com.google.android.gms.cast.CastMediaControlIntent;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.fitness.DataTypes;
import com.google.android.gms.fitness.Fitness;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Iterator;

public class MainScreen extends ActionBarActivity {

    private static final int PORT = 8080;
    private static final String APP_ID = "1C0A4056";
    private static final String TAG = MainScreen.class.getSimpleName();
    private static final boolean DEBUG = false;
    private static final long SCANNING_PERIOD = 10000;
    private static final int REQUEST_ENABLE_BT = 0;

    private Handler mHandler = new Handler();
    private MediaRouter mediaRouter;
    private MediaRouter.Callback mediaRouterCallBack;
    private MediaRouteSelector mediaRouteSelector;
    private MenuItem mediaRouteMenuItem;
    private CastDevice mSelectedDevice;
    private boolean mWaitingForReconnect;
    private ConnectionFailedListener mConnectionFailedListener;
    private GoogleApiClient mApiClient;
    private ConnectionCallbacks mConnectionCallbacks;
    private Cast.Listener mCastClientListener;
    private Receiver receiverChannel;
    private boolean mApplicationStarted;
    private String mSessionId;
    private BluetoothAdapter bluetoothAdapter;
    private BluetoothGatt mBluetoothGatt;
    private BalanceBoard balanceBoard;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_screen);

        mConnectionFailedListener = new ConnectionFailedListener();
        mConnectionCallbacks = new ConnectionCallbacks();

        mediaRouter = MediaRouter.getInstance(getApplicationContext());
        // Create a MediaRouteSelector for the type of routes your app supports
        mediaRouteSelector = new MediaRouteSelector.Builder()
                .addControlCategory(
                        CastMediaControlIntent.categoryForCast(APP_ID)).build();
        // Create a MediaRouter callback for discovery events
        mediaRouterCallBack = new MyMediaRouterCallback();


        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        bluetoothAdapter.startDiscovery();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.main_screen, menu);

        //Attach the MediaRouteSelector to the menu item

        //MenuItem
        mediaRouteMenuItem = menu.findItem(R.id.media_route_menu_item);
        MediaRouteActionProvider mediaRouteActionProvider = (MediaRouteActionProvider) MenuItemCompat.getActionProvider(mediaRouteMenuItem);
        mediaRouteActionProvider.setRouteSelector(mediaRouteSelector);
        System.out.println(mediaRouteMenuItem.getTitle());
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.media_route_menu_item) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void move(View v) {
        switch(v.getId()){
            case R.id.button_up :
                sendMessage("up");
                break;
            case R.id.button_right :
                sendMessage("right");
                break;
            case R.id.button_left :
                sendMessage("left");
                break;
            case R.id.button_replay :
                sendMessage("replay");
                break;
        }
    }

    public void bluetoothFound(BluetoothDevice device){
        bluetoothAdapter.cancelDiscovery();
        balanceBoard = new BalanceBoard(bluetoothAdapter, device, wmListener);
    }

    public void startBtScan(){

        if(bluetoothAdapter == null){
            //Device does not support bluetooth
            return;
        }
        if(!bluetoothAdapter.isEnabled()){
            startActivityForResult(new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE), REQUEST_ENABLE_BT);
            return;
        }
    }

    private final BroadcastReceiver btReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent)
        {
            String action = intent.getAction();
            if (DEBUG)
                Log.d(TAG, "Action " + action);

            if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                if (DEBUG)
                    Log.d(TAG, "Found device " + device.getAddress() + " " + device.getName());
                if ("Nintendo RVL-WBC-01".equals(device.getName())) {
                    if (balanceBoard == null)
                        bluetoothFound(device);
                    else if (DEBUG)
                        Log.d(TAG, "Ignoring board because already have one");
                } else {
                    if (DEBUG)
                        Log.d(TAG, "Ignoring non-balance board device");
                }
            } else if (BluetoothAdapter.ACTION_DISCOVERY_FINISHED.equals(action)) {
                if (DEBUG)
                    Log.d(TAG, "BT scan finished");
                if (balanceBoard == null) {
                    if (DEBUG)
                        Log.d(TAG, "No board connected, begin another scan");
                    startBtScan();
                }
            }
        }
    };

    public void closeGattService(){
        if (mBluetoothGatt == null) {
            return;
        }
        mBluetoothGatt.close();
        mBluetoothGatt = null;
    }

    @SuppressLint("NewApi")
    @Override
    protected void onResume() {
        super.onResume();
        try {
            //SERVER OFFLINE
//            server = new Server(PORT);
//            server.start();
//            System.out.println("SERVER STARTED");
//            System.out.println("IPV4 " + Utils.getIPAddress(true));
        } catch (Exception e) {
            e.printStackTrace();
        }
        mediaRouter.addCallback(mediaRouteSelector, mediaRouterCallBack,
                MediaRouter.CALLBACK_FLAG_REQUEST_DISCOVERY);
    }

    protected void onStart(){
        super.onStart();
    }

    protected void onDestroy(){
        super.onDestroy();
    }

    @Override
    protected void onPause() {
        if (isFinishing()) {
            mediaRouter.removeCallback(mediaRouterCallBack);
        }
        super.onPause();
    }

    private void sendMessage(String message) {
        if (mApiClient != null && receiverChannel != null) {
            try {
                Cast.CastApi.sendMessage(mApiClient,
                        receiverChannel.getNameSpace(), message)
                        .setResultCallback(new ResultCallback<Status>() {
                            @Override
                            public void onResult(Status result) {
                                if (!result.isSuccess()) {
                                    Log.e(TAG, "Sending message failed");
                                    System.out.println("Sending message failed");
                                }
                                else{
                                    System.out.println("Success sending message");
                                }
                            }
                        });
            } catch (Exception e) {
                Log.e(TAG, "Exception while sending message", e);
            }
        } else {
            Toast.makeText(MainScreen.this, message, Toast.LENGTH_SHORT)
                    .show();
        }
    }

    private void teardown() {
        Log.d(TAG, "teardown");
        if (mApiClient != null) {
            if (mApplicationStarted) {
                if (mApiClient.isConnected() || mApiClient.isConnecting()) {
                    try {
                        Cast.CastApi.stopApplication(mApiClient, mSessionId);
                        if (receiverChannel != null) {
                            Cast.CastApi.removeMessageReceivedCallbacks(
                                    mApiClient,
                                    receiverChannel.getNameSpace());
                            receiverChannel = null;
                        }
                    } catch (IOException e) {
                        Log.e(TAG, "Exception while removing channel", e);
                    }
                    mApiClient.disconnect();
                }
                mApplicationStarted = false;
            }
            mApiClient = null;
        }
        mSelectedDevice = null;
        mWaitingForReconnect = false;
        mSessionId = null;
    }

    private void launchReceiver() {
        try {
            mCastClientListener = new Cast.Listener() {
                @Override
                public void onApplicationStatusChanged() {
                    if (mApiClient != null) {
                        Log.d(TAG, "onApplicationStatusChanged: "
                                + Cast.CastApi.getApplicationStatus(mApiClient));
                    }
                }

                @Override
                public void onVolumeChanged() {
                    if (mApiClient != null) {
                        Log.d(TAG, "onVolumeChanged: " + Cast.CastApi.getVolume(mApiClient));
                    }
                }

                @Override
                public void onApplicationDisconnected(int errorCode) {
                    teardown();
                }
            };

            Cast.CastOptions.Builder apiOptionsBuilder = Cast.CastOptions
                    .builder(mSelectedDevice, mCastClientListener);

            mApiClient = new GoogleApiClient.Builder(this)
                    .addApi(Cast.API, apiOptionsBuilder.build())
//                    .addApi(Fitness.API)
//                    .addScope(FitnessScopes.SCOPE_ACTIVITY_READ)
//                    .addScope(FitnessScopes.SCOPE_BODY_READ_WRITE)
                    .addConnectionCallbacks(mConnectionCallbacks)
                    .addOnConnectionFailedListener(mConnectionFailedListener)
                    .build();

            PendingResult<Status> pendingResult = Fitness.RecordingApi.subscribe(mApiClient, DataTypes.ACTIVITY_SAMPLE);

            Status st = pendingResult.await();

            if(st.isSuccess()){
                Log.i(TAG, "Successfully subscribed!");
            }
            else{
                Log.i(TAG, "Subscribing not successful");
            }

            mApiClient.connect();
            System.out.println("Connected to GoogleApiClient");
        } catch (Exception e) {
            System.out.println("Failed to launch receiver, error: " + e);
        }
    }

    private class MyMediaRouterCallback extends MediaRouter.Callback {

        @Override
        public void onRouteSelected(MediaRouter router, MediaRouter.RouteInfo info) {
            Log.d(TAG, "onRouteSelected");
            // Handle route selection.
            mSelectedDevice = CastDevice.getFromBundle(info.getExtras());
            launchReceiver();
        }

        @Override
        public void onRouteUnselected(MediaRouter router, MediaRouter.RouteInfo info) {
            Log.d(TAG, "onRouteUnselected: info=" + info);
            mSelectedDevice = null;
        }
    }

    private class ConnectionCallbacks implements
            GoogleApiClient.ConnectionCallbacks {
        @Override
        public void onConnected(Bundle connectionHint) {
            Log.d(TAG, "onConnected");

            if (mApiClient == null) {
                // We got disconnected while this runnable was pending
                // execution.
                return;
            }

            try {
                if (mWaitingForReconnect) {
                    mWaitingForReconnect = false;

                    // Check if the receiver app is still running
                    if ((connectionHint != null)
                            && connectionHint
                            .getBoolean(Cast.EXTRA_APP_NO_LONGER_RUNNING)) {
                        Log.d(TAG, "App  is no longer running");
                        teardown();
                    } else {
                        // Re-create the custom message channel
                        try {
                            Cast.CastApi.setMessageReceivedCallbacks(
                                    mApiClient,
                                    receiverChannel.getNameSpace(),
                                    receiverChannel);
                        } catch (IOException e) {
                            Log.e(TAG, "Exception while creating channel", e);
                        }
                    }
                } else {
                    // Launch the receiver app
                    Cast.CastApi
                            .launchApplication(mApiClient,
                                    APP_ID, false)
                            .setResultCallback(
                                    new ResultCallback<Cast.ApplicationConnectionResult>() {
                                        @Override
                                        public void onResult(
                                                Cast.ApplicationConnectionResult result) {
                                            Status status = result.getStatus();
                                            Log.d(TAG,
                                                    "ApplicationConnectionResultCallback.onResult: statusCode"
                                                            + status.getStatusCode());
                                            if (status.isSuccess()) {
                                                ApplicationMetadata applicationMetadata = result
                                                        .getApplicationMetadata();
                                                mSessionId = result
                                                        .getSessionId();
                                                String applicationStatus = result
                                                        .getApplicationStatus();
                                                boolean wasLaunched = result
                                                        .getWasLaunched();
                                                Log.d(TAG,
                                                        "application name: "
                                                                + applicationMetadata
                                                                .getName()
                                                                + ", status: "
                                                                + applicationStatus
                                                                + ", sessionId: "
                                                                + mSessionId
                                                                + ", wasLaunched: "
                                                                + wasLaunched);
                                                mApplicationStarted = true;

                                                // Create the custom message
                                                // channel
                                                receiverChannel = new Receiver();
                                                try {
                                                    Cast.CastApi
                                                            .setMessageReceivedCallbacks(
                                                                    mApiClient,
                                                                    receiverChannel
                                                                            .getNameSpace(),
                                                                    receiverChannel);
                                                } catch (IOException e) {
                                                    Log.e(TAG,
                                                            "Exception while creating channel",
                                                            e);
                                                }

                                                // set the initial instructions
                                                // on the receiver
                                            } else {
                                                Log.e(TAG,
                                                        "application could not launch");
                                                teardown();
                                            }
                                        }
                                    });
                }
            } catch (Exception e) {
                Log.e(TAG, "Failed to launch application", e);
            }
        }

        @Override
        public void onConnectionSuspended(int cause) {
            Log.d(TAG, "onConnectionSuspended");
            mWaitingForReconnect = true;
        }
    }


    private class ConnectionFailedListener implements
            GoogleApiClient.OnConnectionFailedListener {
        @Override
        public void onConnectionFailed(ConnectionResult result) {
            teardown();
        }
    }

    class Receiver implements Cast.MessageReceivedCallback {

        public String getNameSpace() {
            return "urn:x-cast:cast.sample.custom.gameView";
        }

        @Override
        public void onMessageReceived(CastDevice castDevice, String namespace, String message) {
            Log.d(TAG, "onMessageReceived: " + message);
        }
    }

    private class LeDeviceListAdapter extends BaseAdapter {

        private ArrayList<BluetoothDevice> mLeDevices;

        public LeDeviceListAdapter(){
            super();
            mLeDevices = new ArrayList<BluetoothDevice>();
        }

        public void addDevice(BluetoothDevice d){
            if(!mLeDevices.contains(d)){
                mLeDevices.add(d);
            }
        }

        public BluetoothDevice getBluetoothDeviceByPosition(int position){
            return mLeDevices.get(position);
        }

        public BluetoothDevice getBluetoothDeviceByDevice(BluetoothDevice d){
            Iterator it = mLeDevices.iterator();
            while(it.hasNext()){
                if(it.next().equals(d)){
                    return d;
                }
            }
            return null;
        }

        public void clear(){
            mLeDevices.clear();
        }

        @Override
        public int getCount() {
            return mLeDevices.size();
        }

        @Override
        public Object getItem(int i) {
            return mLeDevices.get(i);
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            return null;
        }
    }

    private final BalanceBoard.Listener wmListener = new BalanceBoard.Listener() {

        @Override
        public void onWiimoteConnecting(BalanceBoard wm) {

        }

        @Override
        public void onWiimoteConnected(BalanceBoard wm) {

        }

        @Override
        public void onWiimoteDisconnected(BalanceBoard wm) {

        }

        @Override
        public void onWiimoteLEDChange(BalanceBoard wm) {

        }

        @Override
        public void onWiimoteData(BalanceBoard wm, BalanceBoard.Data data) {

        }
    };
}
